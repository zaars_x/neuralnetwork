import numpy as np
import random

NETWORK_FILENAME = r'neural_data.dt'


class NeuralNetwork:
    """Main class for neural network
    """

    # Layouts of neural network
    layouts = None

    def __init__(self, layouts, randomly=False, *args, **kwargs):
        """Initialise
        :param layouts:
        :param randomly:
        :param args:
        :param kwargs:
        """
        self.fill(layouts, randomly, **kwargs)

    # # # Method for uses in class

    # # # In/Out methods

    def fill(self, layouts=None, randomly=False, **kwargs):
        """Fill network by data
        """
        if layouts and type(layouts) is not list:
            raise ValueError('layouts must be list, not %s' % type(layouts))

        self.layouts = layouts
        if randomly:
            self._fill_randomly(**kwargs)

    def _fill_randomly(self, sizes=None, levels=3, min_val=None, max_val=None):
        """Fill created layouts randomly values"""
        min_val = min_val or int(random.random() * 1000 % 1000)
        max_val = max_val or int(random.random() * 1000 % 1000)
        if min_val > max_val:
            min_val, max_val = max_val, min_val

        print(min_val, max_val, sizes)

        # For now fill randomly with sizes
        if sizes and type(sizes) is list:
            layouts = []
            for size in sizes:
                layouts.append([(x / max_val) for x in [random.randint(min_val, max_val) for i in range(size)]])

            self.layouts = np.array(layouts)


    # # # Store/Dumps methods

    def store(self, filename=None):
        if filename:
            with open(filename, 'w') as file:
                for i, layout in enumerate(self.layouts):
                    if i:
                        file.write(';')
                    file.write(','.join([str(item) for item in layout]))
            print('Layouts was stored')
            return True

        print('Storing was canceled')
        return False


    def load(self, filename=None):
        if filename:
            layouts = []
            with open(filename, 'r') as file:
                layouts = np.array([np.array([float(val) for val in line.split(',')]) for line in file.read().split(';')])
            self.layouts = np.array(layouts)
            print('Layouts was loaded')
            return True

        print('Loading was canceled')
        return False

    # # # Calculate methods

    def run(self):
        """Run and calculate results
        """
        for i in range(1, len(self.layouts)):
            self._calc_layout(i)

    def _calc_layout(self, index):
        """Calculate some layout. Layout #0 - is input and no calculating here"""
        if not (0 < index < len(self.layouts)):
            raise IndexError('Index out of range (%d not between %d and %d)' % (index, 0, len(self.layouts)))

network = NeuralNetwork(layouts=[], sizes=[5, 7, 3], randomly=True)
network.store(NETWORK_FILENAME)
network.load(NETWORK_FILENAME)
print(network.layouts)

network.run()
